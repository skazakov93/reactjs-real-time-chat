<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Angular-Laravel App</title>
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">

        <!-- CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

        <!--  -->
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://cdn.rawgit.com/samsonjs/strftime/master/strftime-min.js"></script>
        <script src="//js.pusher.com/3.0/pusher.min.js"></script>
        <!--  -->
    </head>

    <body ng-app="wp-angular-starter">

        <div class="container">
        	<div ui-view></div>
        </div>        
     
    </body>

    <!-- Application Dependencies -->
    <script type="text/javascript" src="../bower_components/angular/angular.min.js"></script>
    <script type="text/javascript" src="../bower_components/angular-ui-router/release/angular-ui-router.js"></script>
    <script type="text/javascript" src="../node_modules/satellizer/satellizer.min.js"></script>
    <script type="text/javascript" src="../bower_components/angular-pusher/angular-pusher.min.js"></script>

    <!-- Application Scripts -->
    <script type="text/javascript" src="../app/app.js"></script>
    <script type="text/javascript" src="../app/controllers/userController.js"></script>
    <script type="text/javascript" src="../app/controllers/newContr.js"></script>
    <script type="text/javascript" src="../app/controllers/authController.js"></script>
    <script type="text/javascript" src="../app/controllers/chatController.js"></script>

    <script type="text/javascript" src="../app/services/commentService.js"></script>
</html>
