<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $users = array(
                ['name' => 'Riste Petrov', 'email' => 'riste.petrov@gmail.com', 'password' => Hash::make('secret')],
                ['name' => 'Angela Nikolovska', 'email' => 'angela@yahoo.com', 'password' => Hash::make('secret')],
                ['name' => 'Ana Arangelova', 'email' => 'ana.angelova@gmail.com', 'password' => Hash::make('secret')],
                ['name' => 'Dejan Stojanov', 'email' => 'dejan_stojanov@yahoo.com', 'password' => Hash::make('secret')],
                ['name' => 'Slavko Kazakov', 'email' => 'slavce_caki@yahoo.com', 'password' => Hash::make('123456')],
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }

        Model::reguard();
    }
}
