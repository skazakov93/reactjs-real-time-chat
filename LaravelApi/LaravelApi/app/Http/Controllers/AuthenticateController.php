<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;
use App\Http\Requests\LoginRequest; 
use Illuminate\Support\Facades\App;

class AuthenticateController extends Controller
{
    var $pusher;

	public function __construct()
    {
        $this->pusher = App::make('pusher');

        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function authenticate(LoginRequest $request){
    	$credentials = Input::only('email', 'password');

    	try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'The Credentials do not match our records!!'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = JWTAuth::toUser($token);

        //za prikazuvanje na notifikacija deka sme ONLINE
        $notifyChannel = 'online';
        $this->pusher->trigger($notifyChannel, 'new-message', $user->id);

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function index()	{
    	//za zemanje na User-ot od prateniot token!!!!
	    $token = JWTAuth::getToken();
	    $user = JWTAuth::toUser($token);
    	
	    // Retrieve all the users in the database and return them
	    $users = User::all();

	    return $users;
	}

    public function getAuthenticatedUser(){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } 
        catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } 
        catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } 
        catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    public function logOutUser(){
        $token = JWTAuth::getToken();

        $token = JWTAuth::refresh($token); 

        return "Token refreshed!!";
    }
}
