<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;
use App\MessageBetween;
use App\User;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Requests\PostMessageRequest;

class ChatController extends Controller
{
    var $pusher;
    var $user;
    var $chatChannel;

    const DEFAULT_CHAT_CHANNEL = 'chat';

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['postAuth']]);

        $this->pusher = App::make('pusher');
        $this->chatChannel = self::DEFAULT_CHAT_CHANNEL;
    }

    public function index()
    {
    	//za zemanje na User-ot od prateniot token!!!!
	    $token = JWTAuth::getToken();
	   	$this->user = JWTAuth::toUser($token); 

	   	$this->chatChannel = 'chat' . $this->user->id;

        return $this->chatChannel;
    }

    public function postMessage(PostMessageRequest $request)
    {
    	//za zemanje na User-ot od prateniot token!!!!
	    $token = JWTAuth::getToken();
	   	$this->user = JWTAuth::toUser($token); 

	   	$messageBaza = Message::create([
	   		'text' => $request->input('chat_text'),
	   		'sender_id' => $this->user->id
	   	]);

	   	$messageBetween = MessageBetween::create([
	   		'sender_id' => $this->user->id,
	   		'receiver_id' => $request->input('receiver_id'),
	   		'message_id' => $messageBaza->id,
	   	]);

        $idArray = [
            $this->user->id,
            $request->input('receiver_id')            
        ];

        rsort($idArray);

        //dd($idArray);

        $myChannel = $idArray[0] . '-' . $idArray[1];

        $this->chatChannel = 'chat' . $myChannel;

        $messegeToReturn = MessageBetween::find($messageBetween->id);
		
        //za prikazuvanje na samata poraka
        $this->pusher->trigger($this->chatChannel, 'new-message', $messegeToReturn);

        //za prikazuvanje na notifikacija deka imame nova poraka
        $notifyChannel = 'chat' . $request->input('receiver_id');
        $this->pusher->trigger($notifyChannel, 'new-message', $messegeToReturn);

        return $this->chatChannel;
    }

    public function getAllusers(){
    	return User::all();
    }

    public function conversationBetween($receiver_id){
		//za zemanje na User-ot od prateniot token!!!!
	    $token = JWTAuth::getToken();
	   	$this->user = JWTAuth::toUser($token);

        $idArray = [
            $this->user->id,
            $receiver_id
        ];

        rsort($idArray);

        $myChannel = $idArray[0] . '-' . $idArray[1];

	   	$this->chatChannel = 'chat' . $myChannel;

        $niza = [
            $receiver_id,
            $this->user->id
        ];

        $dataM = MessageBetween::whereIn('sender_id', $niza)
                                ->whereIn('receiver_id', $niza)
                                ->where('sender_id', '!=', DB::raw('receiver_id'))
                                ->orderBy('created_at', 'desc')
                                ->paginate(20);
        
        $messagesData = $dataM->getCollection()->reverse();

        $returnData = [];
        foreach ($messagesData as $message) {
            $returnData[] = $message;
        }

	   	$data = [
	   		'messages' => $returnData,
	   		'channelname' => $this->chatChannel,
	   	];

	   	return $data;     	
    }

}
