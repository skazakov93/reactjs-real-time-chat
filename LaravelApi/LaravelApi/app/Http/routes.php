<?php
header("Access-Control-Allow-Origin: *");

header("Access-Control-Allow-Methods: DELETE");

header('Access-Control-Allow-Headers: Content-Type, Authorization');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/chat', 'ChatController@index');

Route::post('/chat/message', 'ChatController@postMessage');

Route::post('/pusher/auth', 'ChatController@postAuth');

// API ROUTES ==================================
Route::group(array('prefix' => 'api'), function() {

	Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);

    Route::post('authenticate', 'AuthenticateController@authenticate');

    Route::post('logout', 'AuthenticateController@logOutUser');

    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');

	Route::get('/users', 'ChatController@getAllusers');

	Route::get('/users/{receiver_id}/cenversation', 'ChatController@conversationBetween');    
});
