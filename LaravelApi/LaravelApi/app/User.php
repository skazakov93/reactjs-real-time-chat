<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendMessages(){
        return $this->hasMany('App\Message', 'sender_id', 'id');
    }

    public function messagesBetween(){
        return $this->hasMany('App\MessageBetween', 'receiver_id', 'id');
    }

    public function mySentMessages(){
        return $this->hasMany('App\MessageBetween', 'sender_id', 'id');
    }

    public function myReceivedMessages(){
        return $this->hasMany('App\MessageBetween', 'receiver_id', 'id');
    }
}
