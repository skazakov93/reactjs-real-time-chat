<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	public $timestamps = false;
	
    protected $table = "messages";

    protected $with =[
    	'sender',
    	'messageBetween',
    ];
	
	protected $fillable = [
		'text',
		'sender_id',
	];

	public function sender(){
		return $this->belongsTo('App\User', 'sender_id', 'id');
	}

	public function messageBetween(){
		return $this->belongsTo('App\MessageBetween', 'message_id', 'id');
	}

}
