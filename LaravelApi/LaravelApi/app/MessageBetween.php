<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MessageBetween extends Model
{
    protected $table = "messages_between";

    protected $with = ['message', 'sender'];
	
	protected $fillable = [
		'sender_id',
		'receiver_id',
		'message_id'
	];

	public function getCreatedAtAttribute($value) {
		return Carbon::parse($value)->diffForHumans();
	}

	public function sender(){
		return $this->belongsTo('App\User', 'sender_id', 'id');
	}

	public function receiver(){
		return $this->belongsTo('App\User', 'receiver_id', 'id');
	}

	public function message(){
		return $this->belongsTo('App\Message', 'message_id', 'id');
	}
}
