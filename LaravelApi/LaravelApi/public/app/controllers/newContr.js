angularAppStat.controller('newController1', ['$scope', '$http', '$log', '$location', '$window', 'commentService', '$stateParams',
        function ($scope, $http, $log, $location, $window, commentService, $stateParams) {

    $scope.todos = [];
    $scope.todoById = 0;
    $scope.toDoInputText = "";

    // loading variable to show the spinning loading icon
    $scope.loading = true;

    //bool vrednost za dali da se prikaze error poraka
    $scope.hasError = false;

    var fetchAllToDos = function(){
        $scope.loading = true;

        $http.get(
            'http://localhost:8888/api/todos'
        ).success(function (data, status) {
            if (status == 200) {
                $scope.todos = data;
                $scope.loading = false;
            }
            else {
                $log.info("Server error!");
            }
            //alert("OKOKOK23");
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
            //alert("OKOKOK");
        });
    }

    // var fetchAllToDos = function(){
    //     $scope.loading = true;

    //     $scope.$on('commentService:getToDosService', function() {
    //         $scope.todos = commentService.todos;

    //         $scope.loading = false;

    //         $log.info($scope.todos);
    //     })

    //     $scope.loading = false;
    // };

    fetchAllToDos();
    
    $scope.deleteToDo = function(id){
        // return $http({
        //     method: 'DELETE',
        //     url: 'http://localhost:8888/api/todos/' + id,
        //     headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
        //     //data: $.param(commentData)
        // }).success(function (data) {
        //     fetchAllToDos();
        // });

        $scope.loading = true;

        $http.delete(
            'http://localhost:8888/api/todos/' + id
        )
        .success(function (data) {
            fetchAllToDos();
            $scope.loading = false;
        })
        .error(function (data, status) {
            $log.info("AJAX request failed - POST");
        });
    }

    $scope.createToDo = function(toDoText) {
        $scope.toDoText = "";

        var data1 = {
            text: toDoText
        };
        
        console.log(data1);

        $scope.loading = true;

        return $http({
            method: 'POST',
            url: 'http://localhost:8888/api/todos',
            data: data1
        }).success(function (data){
            console.log(data);

            //za dodavanje na noviot element na krajot od listata
            //$scope.todos.push(data);

            //za dodavanje na noviot element na pocetokot od listata
            $scope.todos.unshift(data);

            $scope.loading = false;

            $scope.hasError = false;
        })
        .error(function (data){
            console.log(data);

            $scope.hasError = true;
            $scope.toDoError = data.text[0];

            $scope.loading = false;
        });
    }

    

    $scope.todoId = -1;

    $scope.todoId = ($stateParams.toDoId) ? parseInt($stateParams.toDoId) : 0;

    var getToDoById = function(){
        $http.get(
            'http://localhost:8888/api/todos/' + $scope.todoId
        ).success(function (data, status) {
            if (status == 200) {
                $scope.todoById = data;

                $log.info("TTTT");
            }
            else {
                $log.info("Server error!");
            }
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
            //alert("OKOKOK");
        });
    }

    if($scope.todoId > 0){
        getToDoById();
    }

}]);