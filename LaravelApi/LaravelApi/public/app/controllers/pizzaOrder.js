angular.module('wp-angular-starter').controller('pizzaOrderController', ['$scope', '$http', '$log', '$window', function ($scope, $http, $log, $window) {
	$scope.allPizzas = [];
	$scope.pizzaType;
	$scope.order = [];
	
	$scope.createOrder = function(pizzaType, clientName, clientAddress) {
		$http.post(
            'http://localhost:8080/servlet-showcase/api/placeOrder/' + pizzaType + '/' + clientName + '/' + clientAddress
        ).success(function (data, status) {
            if (status == 200) {
                $scope.order = data;
				alert("Order created with ID: " + data.orderId)
            }
            else {
                $log.info("Server error!");
            }
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
        });
	}
	
	var fetchAllPizza = function(){
		$http.get(
            'http://localhost:8080/servlet-showcase/api/getPizzas'
        ).success(function (data, status) {
            if (status == 200) {
                $scope.allPizzas = data;
            }
            else {
                $log.info("Server error!");
            }
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
        });
	}
	
	fetchAllPizza();
}]);