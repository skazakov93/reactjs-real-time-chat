angular.module('wp-angular-starter')
    .controller('UserController', ['$scope', '$http', '$log', '$window', '$auth', '$state', function ($scope, $http, $log, $window, $auth, $state) {
    
        
    $scope.users = [];
    $scope.error;

    $scope.getUsers = function() {

        // This request will hit the index method in the AuthenticateController
        // on the Laravel side and will return the list of users
        $http.get('http://localhost:8888/api/authenticate').success(function(users) {
            $scope.users = users;
        }).error(function(error) {
            $scope.error = error;
        });
    }

}]);