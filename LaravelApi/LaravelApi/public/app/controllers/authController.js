angular.module('wp-angular-starter')
    .controller('AuthController', ['$scope', '$http', '$log', '$window', '$auth', '$state', '$rootScope', 
        function ($scope, $http, $log, $window, $auth, $state, $rootScope) {

    $scope.invalidCredidentials = false;
    $scope.emailNotValid = false;
    $scope.passwordRequired = false;

    $scope.invalidError = "";
    $scope.mailError = "";
    $scope.passwordError = "";
    

    $scope.login = function() {

        var credentials = {
            email: $scope.email,
            password: $scope.password
        }

        $auth.login(credentials).then(function() {

            // Return an $http request for the now authenticated
            // user so that we can flatten the promise chain
            return $http.get('http://localhost:8888/api/authenticate/user');

        // Handle errors
        }, function(error) {
            $scope.loginError = true;
            $scope.loginErrorText = error.data.error;

            if(error.status == 401){
                $scope.invalidCredidentials = true;
                $scope.invalidError = error.data.error;
            }
            else if(error.state != 401){
                $scope.invalidCredidentials = false;
                $scope.invalidError = "";
            }

            if(error.data.email != null){
                $scope.emailNotValid = true;
                $scope.mailError = error.data.email[0];
            }
            else{
                $scope.emailNotValid = false;
                $scope.mailError = "";   
            }
            
            if(error.data.password != null){
                $scope.passwordRequired = true;
                $scope.passwordError = error.data.password[0];
            }
            else{
                $scope.passwordRequired = false;
                $scope.passwordError = "";   
            }

        // Because we returned the $http.get request in the $auth.login
        // promise, we can chain the next promise to the end here
        }).then(function(response) {

            // Stringify the returned data to prepare it
            // to go into local storage
            var user = JSON.stringify(response.data.user);

            // Set the stringified user data into local storage
            localStorage.setItem('user', user);

            // The user's authenticated state gets flipped to
            // true so we can now show parts of the UI that rely
            // on the user being logged in
            $rootScope.authenticated = true;

            // Putting the user's data on $rootScope allows
            // us to access it anywhere across the app
            $rootScope.currentUser = response.data.user;

            // Everything worked out so we can now redirect to
            // the users state to view the data
            $state.go('userView');
        });
    }

    $scope.logout = function() {

        $auth.logout().then(function() {

            // Remove the authenticated user from local storage
            localStorage.removeItem('user');

            // Flip authenticated to false so that we no longer
            // show UI elements dependant on the user being logged in
            $rootScope.authenticated = false;

            // Remove the current user info from rootscope
            $rootScope.currentUser = null;

            $state.go('authView');
        });
    }

}]);