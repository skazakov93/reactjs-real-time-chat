angularAppStat.controller('ChatController', ['$scope', '$http', '$log', '$location', '$window', 'commentService', '$stateParams', '$rootScope',
        function ($scope, $http, $log, $location, $window, commentService, $stateParams, $rootScope) {

    $scope.usedChannel = "";
    $scope.users = [];
    $scope.selectedUser = -1;
    $scope.messagesBetween = [];

    //vospostavuvanje na konekcija so ovoj kanal!!!
    //ovoj kanal ke se koristi samo za ovie dva korisnici!!!!
    $scope.pusher = new Pusher('cbc975288d108603fc39');

    // loading variable to show the spinning loading icon
    $scope.loading = true;

    //bool vrednost za dali da se prikaze error poraka
    $scope.hasError = false;

    var registerUser = function(){
        $scope.loading = true;

        $http.get(
            'http://localhost:8888/chat'
        ).success(function (data, status) {
            if (status == 200) {
                $scope.usedChannel = data;

                console.log($scope.usedChannel);

                // var pusher = new Pusher('cbc975288d108603fc39');
                
                // var channel = pusher.subscribe($scope.usedChannel);
                // channel.bind('new-message', addMessage);
            }
            else {
                $log.info("Server error!");
            }
            //alert("OKOKOK23");
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
            //alert("OKOKOK");
        });
    }

    registerUser();

    var getUsers = function(){
        $scope.loading = true;

        $http.get(
            'http://localhost:8888/api/users'
        ).success(function (data, status) {
            if (status == 200) {
                $scope.users = data;

                //vospostavuvanje na konekcija so ovoj kanal!!!
                //ova e mojot kanal kade ke gi dobivam notifikaciite!!!
                var pusher = new Pusher('cbc975288d108603fc39');
                
                var channelName1 = 'chat' + $rootScope.currentUser.id;

                var channel = pusher.subscribe(channelName1);
                channel.bind('new-message', notifyFunction);
            }
            else {
                $log.info("Server error!");
            }
            //alert("OKOKOK23");
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
            //alert("OKOKOK");
        });
    }

    getUsers();

    $scope.showConversation = function(userId){
        $scope.loading = true;

        $scope.selectedUser = userId;

        $http.get(
            'http://localhost:8888/api/users/' + userId + '/cenversation'
        ).success(function (data, status) {
            if (status == 200) {
                //brisenje na starite poraki koi se zemeni preku pusher (web-sockets)
                clearMessages();

                //prikazuvanje veke postockata konverzacija pomegu dvajcata!!
                $scope.messagesBetween = data.messages;

                //zacuvuvanje na usedChannel
                $scope.usedChannel = data.channelname;

                //oznacuvanje deka porakata e procitana!!!
                for(var i = 0; i < $scope.users.length; i++){
                    if($scope.users[i].id == data.messages[0].receiver_id){
                        //$scope.users[i].name = $scope.users[i].name + " New Message";
                        $scope.users[i].newMessage = null;
                    }
                }

                
                
                //za da ne se primaat duplikat poraki
                //dokolu klikneme na istiot korisnik poveke od dva pati
                //bez da ja refresirame staranata
                $scope.pusher.unsubscribe($scope.usedChannel);

                var channel = $scope.pusher.subscribe($scope.usedChannel);
                channel.bind('new-message', addMessage);
            }
            else {
                $log.info("Server error!");
            }
            //alert("OKOKOK23");
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
            //alert("OKOKOK");
        });
    }
    
    $scope.postMessage = function() {
        var data1 = {
            chat_text: $scope.messageText,
            receiver_id: $scope.selectedUser
        };

        $scope.messageText = "";
        
        console.log(data1);

        $scope.loading = true;

        return $http({
            method: 'POST',
            url: 'http://localhost:8888/chat/message',
            data: data1
        }).success(function (data){
            console.log(data);
        })
        .error(function (data){
            console.log(data);

            
        });
    }

    function notifyFunction(data){
        for(var i = 0; i < $scope.users.length; i++){
            if($scope.users[i].name == data.username && $scope.users[i].id != $scope.selectedUser){
                $scope.users[i].newMessage = "New Message";
            }
        }

        $scope.$apply();
    };

}]);