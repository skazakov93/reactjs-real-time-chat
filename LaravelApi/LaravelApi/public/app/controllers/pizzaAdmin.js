angular.module('wp-angular-starter').controller('pizzaAdminController', ['$scope', '$http', '$log', '$window', function ($scope, $http, $log, $window) {
	$scope.allPizzas = [];
	
	$scope.addPizza = function(pizzaType){
		$http.post(
            'http://localhost:8080/servlet-showcase/api/addPizza/' + pizzaType
        ).success(function (data, status) {
            if (status == 201) {
				alert("Pizza added!");
                $window.location = '/views/pizzaAdmin.html';
            }
            else {
				alert("Pizza can not be added!");
                $log.info("Server error!");
            }
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
        });
	}
	
	$scope.updatePizza = function(pizzaID, pizzaType){
		$http.put(
            'http://localhost:8080/servlet-showcase/api/updatePizza/' + pizzaID + '/' + pizzaType
        ).success(function (data, status) {
            if (status == 200) {
				alert("Pizza updated!");
                $window.location = '/views/pizzaAdmin.html';
            }
            else {
				alert("Pizza can not be updated! It may not exist in database!");
                $log.info("Server error!");
            }
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
        });
	}
	
	$scope.deletePizza = function(pizzaID){
		$http.delete(
            'http://localhost:8080/servlet-showcase/api/deletePizza/' + pizzaID
        ).success(function (data, status) {
            if (status == 200) {
				alert("Pizza deleted!");
                $window.location = '/views/pizzaAdmin.html';
            }
            else {
				alert("Pizza can not be deleted! It may not exist in database!");
                $log.info("Server error!");
            }
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
        });
	}
	
	var fetchAllPizza = function(){
		$http.get(
            'http://localhost:8080/servlet-showcase/api/getPizzas'
        ).success(function (data, status) {
            if (status == 200) {
                $scope.allPizzas = data;
            }
            else {
                $log.info("Server error!");
            }
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
        });
	}
	
	fetchAllPizza();
}]);