angular.module('wp-angular-starter', ['smart-table']).controller('orderAdminController', ['$scope', '$http', '$log', '$window', '$filter', function ($scope, $http, $log, $window, filter) {
	$scope.allOrders = [];
	$scope.displayCollection = [].concat($scope.allOrders);
	
	$scope.itemsByPage = 10;
	
	var fetchAllOrders = function(){
		$http.get(
            'http://localhost:8080/servlet-showcase/api/getOrders'
        ).success(function (data, status) {
            if (status == 200) {
                $scope.allOrders = data;
            }
            else {
                $log.info("Server error!");
            }
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
        });
	}
	
	fetchAllOrders();
}]).filter('ordersFilter', function($filter){
	return function(input, predicate){
        return $filter('filter')(input, predicate, true);
    }
});