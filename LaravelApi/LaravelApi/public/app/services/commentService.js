angularAppStat.service('commentService', ['$http', '$log', '$rootScope', function($http, $log, $rootScope) {

    var self = this;

    self.todos = {};

    self.fetchToDos = function () {
        $http.get(
            'http://localhost:8888/api/todos'
        ).success(function (data, status) {
            if (status == 200) {
                self.todos = data;
                
                $rootScope.$broadcast('commentService:getToDosService');
            }
            else {
                $log.info("Server error!");
            }
            //alert("OKOKOK23");
        })
        .error(function (data, status) {
            $log.info("AJAX request failed");
            //alert("OKOKOK");
        });
    };

    self.fetchToDos();

}]);