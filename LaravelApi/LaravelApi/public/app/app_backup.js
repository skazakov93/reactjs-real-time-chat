var WPAngularStarter = angular.module('wp-angular-starter', [
  'ui.router',
  'ngResource',
  'pascalprecht.translate',
  'smart-table',
  'mgcrea.ngStrap',
  'toastr',
  'angular-loading-bar',
  'ui.select',
  'ngQuickDate']);

WPAngularStarter.config(function($routeProvider) {
	$routeProvider.when('/pizza-order', {
		controller : 'pizzaOrderController',
		templateUrl : 'views/orderPizza.html'
		}).when('/pizza-type-admin', {
		controller : 'UsersByIdCtrl',
		templateUrl : 'views/userbyid.html'
		}).when('/order-admin', {
		controller : 'UsersCtrl',
		templateUrl : 'views/users.html'
		}).otherwise({
		controller : 'SpaCtrl',
		templateUrl: 'views/spahome.html'
		});
});