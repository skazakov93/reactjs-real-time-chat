import request from 'reqwest';
import when from 'when';

import GetUsersActions from '../actions/GetUsersActions';
import LoginStore from '../stores/LoginStore.js';

class GetUsersService {

  getUsers() {
    request({
      url: 'http://localhost:8000/api/authenticate',
      method: 'GET',
      crossOrigin: true,
      headers: {
        'Authorization': 'Bearer ' + LoginStore.jwt
      }
    })
    .then(function(response) {
      GetUsersActions.getUsers(response);
    });
  }

  notifyFunction(users){
      for(var i = 0; i < users.length; i++){
          if(users[i].name == data.username && users[i].id != $scope.selectedUser){
              users[i].newMessage = "New Message";
          }
      }
  };

}

export default new GetUsersService()