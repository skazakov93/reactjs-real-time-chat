import request from 'reqwest';
import when from 'when';

import ChatActions from '../actions/ChatActions';
import LoginStore from '../stores/LoginStore';

class ChatService {

    getUsers() {
        request({
          url: 'http://localhost:8000/api/users',
          method: 'GET',
          crossOrigin: true,
          headers: {
            'Authorization': 'Bearer ' + LoginStore.jwt
          }
        })
        .then(function(response) {
          ChatActions.getUsers(response);
        });
    }

    showConversation(userId, pageNumber) {
        request({
          url: 'http://localhost:8000/api/users/' + userId + '/cenversation?page=' + pageNumber,
          method: 'GET',
          crossOrigin: true,
          headers: {
            'Authorization': 'Bearer ' + LoginStore.jwt
          }
        })
        .then(function(response) {
          ChatActions.choosenUser(response.channelname);
          ChatActions.getMessagesBetween(response.messages);
        });
    }

    postMessage(data) {
        request({
          url: 'http://localhost:8000/chat/message',
          method: 'POST',
          data: data,
          crossOrigin: true,
          headers: {
            'Authorization': 'Bearer ' + LoginStore.jwt
          }
        })
        .then(function(response) {
          
        });
    }

}

export default new ChatService()