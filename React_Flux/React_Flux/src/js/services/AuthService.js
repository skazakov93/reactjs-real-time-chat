import request from 'reqwest';
import when from 'when';
import {LOGIN_URL, SIGNUP_URL} from '../constants/LoginConstants';
import LoginActions from '../actions/LoginActions';
import LoginStore from '../stores/LoginStore';

class AuthService {

  login(email, password) {
    return this.handleAuth(when(request({
      url: 'http://localhost:8000/api/authenticate',
      method: 'POST',
      crossOrigin: true,
      type: 'json',
      data: {
        email, password
      }
    })));
  }

  logout() {
    LoginActions.logoutUser();
  }

  signup(email, password, extra) {
    return this.handleAuth(when(request({
      url: SIGNUP_URL,
      method: 'POST',
      crossOrigin: true,
      type: 'json',
      data: {
        email, password, extra
      }
    })));
  }

  handleAuth(loginPromise) {
    return loginPromise
      .then(function(response) {
        console.log("RESPONSE od AUTH");
        var jwt = response.token;
        LoginActions.loginUser(jwt);
        return true;
      });
  }

  getUserInfo() {
    request({
          url: 'http://localhost:8000/api/authenticate/user',
          method: 'GET',
          crossOrigin: true,
          headers: {
            'Authorization': 'Bearer ' + LoginStore.jwt
          }
        })
        .then(function(response) {
          LoginActions.saveUserLocalStorage(response.user);
        });
  }

  logOutUser() {
    request({
          url: 'http://localhost:8000/api/logout',
          method: 'POST',
          crossOrigin: true,
          headers: {
            'Authorization': 'Bearer ' + LoginStore.jwt
          }
        })
        .then(function(response) {
          LoginActions.logoutUser();
        });
  }
}

export default new AuthService()