import React from "react";

import AuthService from '../services/AuthService'

export default class LogOut extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        AuthService.logOutUser();
    }

    componentWillUnmount() {
        
    }

    render() {
      	return (
            <div>
            </div>
        );
    }
}