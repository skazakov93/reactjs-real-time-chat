import React from "react";

import GetUsersComponent from '../components/GetUsersComponent';
import MessagesBetween from '../components/MessagesBetween';
import SendMessageComponent from '../components/SendMessageComponent';
import ChatStore from '../stores/ChatStore';
import LoginStore from '../stores/LoginStore';

export default class Chat extends React.Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            choosenUserId: '',
            currentUser: ''
        };

        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        ChatStore.addChangeListener(this._onChange);
        LoginStore.addChangeListener(this._onChange);

        this.setState(this.getUserName());
    }

    componentWillUnmount() {
        ChatStore.removeChangeListener(this._onChange);
        LoginStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(this.getChoosenUserId());
    }

    updateEmail(ev) {
        this.setState({ email: ev.target.value });
    }

    updatePassword(ev) {
        this.setState({ password: ev.target.value });
    }

    getChoosenUserId() {
        return {
          choosenUserId: ChatStore.choosenUserId
        };
    }

    getUserName() {
        return {
          currentUser: LoginStore.currentUserName
        };   
    }

    render() {
      	return (
            <div>
                <div class="stripe no-padding-bottom numbered-stripe">
                    <div class="fixed wrapper">
                        <ol class="strong">
                            <li>
                                <div class="hexagon"></div>
                                <h2><b>Real-Time Chat</b> <small></small> Hello, { this.state.currentUser }</h2>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="blue-gradient-background">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <GetUsersComponent />
                            </div>

                            <div class="col-md-8 chat-class">
                                <div class="row light-grey-blue-background chat-app">

                                    <div id="messages">
                                        <div class="time-divide">
                                            <span class="date">Today</span>
                                        </div>

                                        <MessagesBetween />
                                    </div>

                                    <SendMessageComponent choosenUserId={ this.state.choosenUserId } />
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="chatChannelId" />
                </div>
            </div>
        );
    }
}