import React from "react";
import AuthService from '../services/AuthService'

export default class SignIn extends React.Component {
  constructor() {
    super()
    this.state = {
      email: '',
      password: ''
    };
  }

  login(e) {
    e.preventDefault();
    
    AuthService.login(this.state.email, this.state.password)
      .catch(function(err) {
        alert("There's an error logging in");
        console.log("Error logging in", err);
      });
  }

  updateEmail(ev) {
    this.setState({ email: ev.target.value });
  }

  updatePassword(ev) {
    this.setState({ password: ev.target.value });
  }

  render() {
  	return (
      	<div className="login jumbotron center-block">
		    <h1>Login</h1>
		    <form role="form">
		    <div className="form-group">
		      <label htmlFor="username">Username</label>
		      <input type="text" onChange={this.updateEmail.bind(this)} className="form-control" id="username" placeholder="Username" />
		    </div>
		    <div className="form-group">
		      <label htmlFor="password">Password</label>
		      <input type="password" onChange={this.updatePassword.bind(this)} className="form-control" id="password" ref="password" placeholder="Password" />
		    </div>
		    <button type="submit" className="btn btn-default" onClick={this.login.bind(this)}>Submit</button>
		  </form>
		</div>
    );
  }
}