import dispatcher from "../dispatcher";
import {LOGIN_USER, LOGOUT_USER} from '../constants/LoginConstants.js';
import RouterContainer from '../services/RouterContainer';
import React from "react";
import AuthService from '../services/AuthService';

export default {
  loginUser: (jwt) => {
    var savedJwt = localStorage.getItem('jwt');
    
    dispatcher.dispatch({
      actionType: LOGIN_USER,
      jwt: jwt
    });

    // Zemanje na Info za Najaveniot user!!!
    AuthService.getUserInfo();
    
    if (savedJwt !== jwt) {
      localStorage.setItem('jwt', jwt);

      // ToDo redirect to index
    }
  },

  logoutUser: () => {
    localStorage.removeItem('jwt');
    dispatcher.dispatch({
      actionType: LOGOUT_USER
    });
  },

  saveUserLocalStorage: (userData) => {
    var userStorage = localStorage.getItem('user');

    dispatcher.dispatch({
      actionType: 'USER_DATA_FETCH',
      userData: userData.name
    });
    
    if (userStorage !== userData) {
      localStorage.setItem('user', JSON.stringify(userData));

      // ToDo redirect to index
    }
  },
}