import dispatcher from "../dispatcher";

export default {
  getUsers: (users) => {
    dispatcher.dispatch({
      actionType: 'USERS_FETCHING_DONE',
      users: users
    })
  }
}