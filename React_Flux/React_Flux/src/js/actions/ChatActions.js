import dispatcher from "../dispatcher";

export default {
  getUsers: (users) => {
    dispatcher.dispatch({
      actionType: 'CHAT_USERS_FETCHING_DONE',
      users: users
    })
  },

  getMessagesBetween: (messagesBetween) => {
    dispatcher.dispatch({
      actionType: 'MESSAGES_BETWEEN_FETCHING_DONE',
      messagesBetween: messagesBetween
    })
  },

  setChoosenUser: (choosenUserId) => {
    dispatcher.dispatch({
      actionType: 'USER_CHANGED',
      choosenUserId: choosenUserId
    })
  },

  choosenUser: (choosenUser) => {
    dispatcher.dispatch({
      actionType: 'CHOOSEN_USER_CHANNEL',
      choosenUser: choosenUser
    })
  }
}