import {LOGIN_USER, LOGOUT_USER} from '../constants/LoginConstants';
import BaseStore from './BaseStore';
import jwt_decode from 'jwt-decode';
import { Router, Route, IndexRoute, browserHistory } from "react-router";


class LoginStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(() => this._registerToActions.bind(this))
    this._user = '';
    this._jwt = localStorage.getItem('jwt');

    var temp = localStorage.getItem('user');
    var tempObject = JSON.parse(temp);

    this._currenUserName = '';

    if(tempObject) {
      this._currenUserName = tempObject.name;      
    }
  }

  _registerToActions(action) {
    switch(action.actionType) {
      case LOGIN_USER:
        this._jwt = action.jwt;
        this._user = jwt_decode(this._jwt);
        this.emitChange();
        break;

      case LOGOUT_USER:
        this._user = null;
        this.emitChange();
        break;

      case 'USER_DATA_FETCH':
        this._currenUserName = action.userData;
        this.emitChange();
        break;

      default:
        break;
    };
  }

  get user() {
    return this._user;
  }

  get jwt() {
    return this._jwt;
  }

  isLoggedIn() {
    return !!this._user;
  }

  get currentUserName() {
    return this._currenUserName;
  }
}

export default new LoginStore();