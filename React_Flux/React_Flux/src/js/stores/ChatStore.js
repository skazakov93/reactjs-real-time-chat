import BaseStore from './BaseStore';

class ChatStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(() => this._registerToActions.bind(this))
    this._users = null;
    this._messagesBetween = null;
    this._choosenUser = null;
    this._choosenUserChannel = null;
  }

  _registerToActions(action) {
    switch(action.actionType) {
      case 'CHAT_USERS_FETCHING_DONE':
        this._users = action.users;
        this.emitChange();
        break;
      
      case 'MESSAGES_BETWEEN_FETCHING_DONE':
        this._messagesBetween = action.messagesBetween;
        this.emitChange();
        break;
      
      case 'USER_CHANGED':
        this._choosenUser = action.choosenUserId;
        this.emitChange();
        break;

      case 'CHOOSEN_USER_CHANNEL':
        this._choosenUserChannel = action.choosenUser;
        //this.emitChange();
        break;

      default:
        break;
    };
  }

  get users() {
    return this._users;
  }

  get messagesBetween() {
    return this._messagesBetween;
  }

  get choosenUserId() {
    return this._choosenUser;
  }

  get choosenUserChannel() {
    return this._choosenUserChannel;
  }

}

export default new ChatStore();