import BaseStore from './BaseStore';

class GetUsersStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(() => this._registerToActions.bind(this))
    this._users = null;
  }

  _registerToActions(action) {
    switch(action.actionType) {
      case 'USERS_FETCHING_DONE':
        this._users = action.users;
        this.emitChange();
        break;
      case 'LOGOUT_USER':
        this._users = null;
        this.emitChange();
        break;
      default:
        break;
    };
  }

  get users() {
    return this._users;
  }
}

export default new GetUsersStore();