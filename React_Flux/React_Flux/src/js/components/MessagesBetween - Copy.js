import React from "react";

import ChatStore from '../stores/ChatStore';
import ChatService from '../services/ChatService';

export default class MessagesBetween extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.getMessagesBetweenState();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        if (!this.state.messagesBetween) {
          this.requestGetMessagesBetween();
        }

        ChatStore.addChangeListener(this._onChange);
        
        // Attach scroll event to the window for infinity paging
        window.addEventListener('scroll', this.checkWindowScroll);
    }

    componentWillUnmount() {
        ChatStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(this.getMessagesBetweenState());
        this.setState(this.getChoosenUserChannel());
        this.setState(this.getMyInitialState());
        this.registerToPusher();
    }

    requestGetMessagesBetween() {
        ChatService.showConversation(ChatStore.choosenUserId, 1);
    }

    getMessagesBetweenState() {
        return {
          messagesBetween: ChatStore.messagesBetween
        };
    }

    getChoosenUserChannel() {
        return {
          choosenUserChannel: ChatStore.choosenUserChannel
        };   
    }

    registerToPusher() {
        //console.log("UUUUU");
        //console.log(this.state.choosenUserChannel);
        
        var mySelf = this;

        var pusher = new Pusher('cbc975288d108603fc39');

        pusher.unsubscribe(this.state.choosenUserChannel);

        var channel = pusher.subscribe(this.state.choosenUserChannel);
        channel.bind('new-message', function (data) {
            var newArray = mySelf.state.messagesBetween;    
            newArray.push(data);   
            mySelf.setState({ messagesBetween: newArray });
            
            // Make sure the incoming message is shown
            //messages.scrollTop(messages[0].scrollHeight);
        });
    }

    ///////////Infinite Scrolling!!
    // Method to load tweets fetched from the server
    loadPagedTweets(tweets){

        // So meta lol
        var self = this;

        // If we still have tweets...
        if(tweets.length > 0) {
            // Get current application state
            var updated = this.state.tweets;

            // Push them onto the end of the current tweets array
            tweets.forEach(function(tweet){
                updated.push(tweet);
            });

            // This app is so fast, I actually use a timeout for dramatic effect
            // Otherwise you'd never see our super sexy loader svg
            setTimeout(function(){

                // Set application state (Not paging, add tweets)
                self.setState({tweets: updated, paging: false});

            }, 1000);
        } 
        else {
            // Set application state (Not paging, paging complete)
            this.setState({done: true, paging: false});
        }
    }

    // Method to check if more tweets should be loaded, by scroll position
    checkWindowScroll(){
        // Get scroll pos & window data
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        var s = (document.body.scrollTop || document.documentElement.scrollTop || 0);
        var scrolled = (h + s) > document.body.offsetHeight;

        console.log("JEEEJ");

        // If scrolled enough, not currently paging and not complete...
        if(scrolled && !this.state.paging && !this.state.done) {
            console.log("JEEEJ22");
            // Set application state (Paging, Increment page)
            this.setState({paging: true, page: this.state.page + 1});

            // Get the next page of tweets from the server
            this.getPage(this.state.page);
        }
    }

    // Set the initial component state
    getMyInitialState(){
        // Set initial application state using props
        return {
            count: 0,
            page: 1,
            paging: false,
            skip: 0,
            done: false
        };
    }

    ////////////////////////////////

    render() {
        var MessagesBetweenComponents = null;

        if(this.state.messagesBetween) {
          MessagesBetweenComponents = this.state.messagesBetween.map((message) => {
            return <div class="message">
                        <div class="avatar">
                            <img src="" />
                        </div>
                        <div class="text-display">
                            <div class="message-data">
                                <span class="author">{ message.sender.name }</span>
                                <span class="timestamp">{ message.created_at }</span>
                                <span class="seen"></span>
                            </div>
                            <p class="message-body">{ message.message.text }</p>
                        </div>
                    </div>
        });
        }
        
        return (
            <ul>
                { MessagesBetweenComponents }    
            </ul>
        );
    }

}
