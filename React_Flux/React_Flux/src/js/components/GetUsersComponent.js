import React from "react";

import GetUsersStore from '../stores/GetUsersStore';
import GetUsersService from '../services/GetUsersService';
import ChatService from '../services/ChatService';
import ChatActions from '../actions/ChatActions';
import ChatStore from '../stores/ChatStore';

export default class GetUsersComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.getUsersState();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        if (!this.state.users) {
          this.requestGetUsers();
        }

        GetUsersStore.addChangeListener(this._onChange);
        //GetUsersStore.addChangeListener(this.requestGetMessagesBetween);
    }

    componentWillUnmount() {
        GetUsersStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(this.getUsersState());
        this.registerForPusherNotifications();
        this.watchForOnlineUSers();
    }

    requestGetUsers() {
        GetUsersService.getUsers();
    }

    getUsersState() {
        return {
          users: GetUsersStore.users
        };
    }

    requestGetMessagesBetween(choosenUserId) {
        ChatActions.setChoosenUser(choosenUserId);

        ChatService.showConversation(choosenUserId, 1);

        var newArray = this.state.users;
        for(var i = 0; i < newArray.length; i++){
            if(newArray[i].id == choosenUserId){
                newArray[i].newMessage = "";
                this.setState({ users: newArray });

                break;
            }
        }
    }

    registerForPusherNotifications() {
      var userStorage = JSON.parse(localStorage.getItem('user'));
      var pusher = new Pusher('cbc975288d108603fc39');
                
      var channelName1 = 'chat' + userStorage.id;

      var mySelf = this;

      var channel = pusher.subscribe(channelName1);
      channel.bind('new-message', function (data) {
          var newArray = mySelf.state.users;
          
          for(var i = 0; i < mySelf.state.users.length; i++){
              if(mySelf.state.users[i].id == data.sender_id && data.sender_id != ChatStore.choosenUserId){
                  newArray[i].newMessage = "New Message";

                  mySelf.setState({ users: newArray });
              }
          }
      });
    }

    watchForOnlineUSers() {
      var pusher = new Pusher('cbc975288d108603fc39');

      var mySelf = this;

      var channel = pusher.subscribe('online');
      channel.bind('new-message', function (data) {
          var newArray = mySelf.state.users;

          for(var j = 0; j < mySelf.state.users.length; j++){
              if(mySelf.state.users[j].id == data){
                  newArray[j].isOnline = "1";

                  mySelf.setState({ users: newArray });
              }
          }
      });

    }

    render() {
        var UsersComponents = null;

        if(this.state.users) {
          UsersComponents = this.state.users.map((user) => {

            let online = <span></span>
            if(user.isOnline == 1) {
              online = <img src='../../images/status_online.png' class="online-class" />
            }

            return <li>
                        <a class="user-list" onClick={this.requestGetMessagesBetween.bind(this, user.id)}>{ user.name } 
                            { online }
                            <span class="badge">{ user.newMessage }</span>
                        </a>
                    </li>;
        });
        }
        
        return (
            <div class="row light-grey-blue-background">
                <ul>
                    { UsersComponents }    
                </ul>
            </div>
        );
    }

}
