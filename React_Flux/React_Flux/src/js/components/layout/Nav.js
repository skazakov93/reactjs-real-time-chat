import React from "react";
import { IndexLink, Link } from "react-router";

export default class Nav extends React.Component {
  constructor() {
    super()
    this.state = {
      collapsed: true,
    };
  }

  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  render() {
    const { location } = this.props;
    const { collapsed } = this.state;
    var signInClass = location.pathname === "/" ? "active" : "";
    signInClass = location.pathname.match(/^\/sign-in/) ? "active" : "";
    const chatClass = location.pathname.match(/^\/chat/) ? "active" : "";
    const logOutClass = location.pathname.match(/^\/logout/) ? "active" : "";
    const navClass = collapsed ? "collapse" : "";

    return (
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" onClick={this.toggleCollapse.bind(this)} >
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class={signInClass}>
                <Link to="sign-in" onClick={this.toggleCollapse.bind(this)}>Sign In</Link>
              </li>
              <li class={chatClass}>
                <Link to="chat" onClick={this.toggleCollapse.bind(this)}>Chat App</Link>
              </li>
              <li class={logOutClass}>
                <Link to="logout" onClick={this.toggleCollapse.bind(this)}>Log Out</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
