import React from "react";
import ReactDOM from "react-dom";
import Waypoint from 'react-waypoint';

import ChatStore from '../stores/ChatStore';
import ChatService from '../services/ChatService';

export default class MessagesBetween extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = this.getMessagesBetweenState();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        if (!this.state.messagesBetween) {
          this.requestGetMessagesBetween();
        }

        ChatStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        ChatStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(this.getMessagesBetweenState());
        this.setState(this.getChoosenUserChannel());
        this.scrollToLastMessage();
        this.registerToPusher();
    }

    requestGetMessagesBetween(pageNumber) {
        ChatService.showConversation(ChatStore.choosenUserId, pageNumber);
    }

    scrollToLastMessage() {
        // Scroll to the bottom on initialization
        var len = this.state.messagesBetween.length - 1;
        const node = ReactDOM.findDOMNode(this['_div' + len]);
        if (node) {
          node.scrollIntoView();
        }
    }

    getMessagesBetweenState() {
        return {
          messagesBetween: ChatStore.messagesBetween,
          choosenUserChannel: 'test',
          pageNumber: 1,
          isInfiniteLoading: false,
          isLoading: false
        };
    }

    getChoosenUserChannel() {
        return {
          choosenUserChannel: ChatStore.choosenUserChannel
        };   
    }

    registerToPusher() {
        console.log("UUUUU");
        // console.log(this.state.choosenUserChannel);
        
        var mySelf = this;

        var pusher = new Pusher('cbc975288d108603fc39');

        var pom = this.state.choosenUserChannel;

        pusher.unsubscribe(pom);

        var channel = pusher.subscribe(pom);
        
        channel.bind('new-message', function (data) {
            var newArray = mySelf.state.messagesBetween;    
            newArray.push(data);   
            mySelf.setState({ messagesBetween: newArray });

            // Make sure the incoming message is shown
            //mySelf.scrollToLastMessage;
        });
    }

    _renderItems() {
      console.log("Render!!!");
      if(this.state.messagesBetween) {
        return this.state.messagesBetween.map((message) => {
          <div class="message">
              <div class="avatar">
                  <img src="" />
              </div>
              <div class="text-display">
                  <div class="message-data">
                      <span class="author">{ message.sender.name }</span>
                      <span class="timestamp">{ message.created_at }</span>
                      <span class="seen"></span>
                  </div>
                  <p class="message-body">{ message.message.text }</p>
              </div>
          </div>
        });  
      }
    }

    ///////////Infinite Scrolling!!
    _loadMoreItems() {
      this.setState({ loading: true });
      // Do the fetching of data here with AJAX
      // In this fake example we just generate more image urls
      // and set the state of 'loading' to false.
      // ...
      console.log("SCROLLING !!!");
    }

    _renderWaypoint() {
      if (!this.state.isLoading) {
        return (
          <Waypoint
            onEnter={this._loadMoreItems}
            threshold={2.0}
          />
        );
      }
    }

    ////////////////////////////////

    render() {
        var MessagesBetweenComponents = null;

        if(this.state.messagesBetween) {
          MessagesBetweenComponents = this.state.messagesBetween.map((message, idx) => {
            return <div class="message" ref={(ref) => this['_div' + idx] = ref} >
                        <div class="avatar">
                            <img src="" />
                        </div>
                        <div class="text-display">
                            <div class="message-data">
                                <span class="author">{ message.sender.name }</span>
                                <span class="timestamp">{ message.created_at }</span>
                                <span class="seen"></span>
                            </div>
                            <p class="message-body">{ message.message.text }</p>
                        </div>
                    </div>
          });
        }
        
        return (
            <ul>
                { this._renderWaypoint() }
                { MessagesBetweenComponents }
            </ul>
        );
    }

} 