import React from "react";

import ChatService from '../services/ChatService';

export default class SendMessageComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          messageText: ''
        };
    }

    updateText(ev) {
        this.setState({ messageText: ev.target.value });
    }

    sendMessage() {
        var data = {
            chat_text: this.state.messageText,
            receiver_id: this.props.choosenUserId
        }

        this.setState({ messageText: '' });
        
        ChatService.postMessage(data);
    }

    render() {
        return (
            <div class="action-bar">
                <input type="text" onChange={this.updateText.bind(this)} value={ this.state.messageText } class="input-message col-xs-10" placeholder="Your message" />
                <div class="option col-xs-1 white-background">
                    <span class="fa fa-smile-o light-grey"></span>
                </div>
                <div onClick={this.sendMessage.bind(this)} class="option col-xs-1 green-background send-message" id="mySendBtn">
                    <span class="white light fa fa-paper-plane-o"></span>
                </div>
            </div>
        );
    }

}
