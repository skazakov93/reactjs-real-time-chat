import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Layout from "./pages/Layout";
import SignIn from "./pages/SignIn";
import Chat from "./pages/Chat";
import LogOut from "./pages/LogOut";

const app = document.getElementById('app');

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={Layout}>
      <IndexRoute component={SignIn}></IndexRoute>
      <Route path="sign-in" component={SignIn}></Route>
      <Route path="chat" component={Chat}></Route>
      <Route path="logout" component={LogOut}></Route>
    </Route>
  </Router>,
app);
